<?php
    if (isset($_POST['delete'])) {
        require_once '../database/database.php';
        $name_comp = clean_string($_POST['name_comp']);
        $db = db_connect();
        if(check($name_comp)) delete_catalog($name_comp);
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Удаление</title>
</head>
<body>
    <form method = "POST" align = "left">
        Введите название компании <br/><input type="text" name="name_comp"><br/><br/>
        <input type="submit" name="delete" value = "Удалить компанию"><br/><br/>
        <a href="/"><input type="button" value = "На главную"></a>
    </form>
</body>
</html>
