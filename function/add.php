<?php
    if (isset($_POST['insert'])){
        require_once '../database/database.php';
        $name = clean_string($_POST['name']); $adress = clean_string($_POST['adress']);
        $number = (int)$_POST['number']; $web_a = clean_string($_POST['web_a']);
        $about_c = clean_string($_POST['about_c']); $fio_dir = clean_string($_POST['fio']);
        if ($name == NULL || $adress == NULL || $number == NULL || $web_a == NULL || $about_c == NULL || $fio_dir == NULL){?>
           <script>alert('Проверьте правильность введёных данных и повторите попытку')</script>
       <?php }
        else {
            add_catalog($name, $adress, $number, $web_a, $about_c, $fio_dir);
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Добавление компании</title>
</head>
<body>
	<form method = "POST" align = "left" >
            Название Компании <input type="text" name="name"><br/><br/>
            Адресс <input type="text" name="adress"><br/><br/>
            Телефон <input type="text" name="number"><br/><br/>
            Адресс сайта <input type="text" name="web_a"><br/><br/>
            О компании: <input type="text" name="about_c"><br/><br/>
            ФИО директора <input type="text" name="fio"><br/><br/>
            <input type="submit" value="Добавить" name="insert"><br/><br/>
	</form>
	<a href ="/"><input type = "button" value = "На главную"></a>
</body>
</html>