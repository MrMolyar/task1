<?php
    require_once '../database/database.php';
    $name_comp = clean_string($_POST['name_comp']); $nameC = clean_string($_POST['nameC']);
    $adress = clean_string($_POST['adress']); $number = (int)$_POST['number'];
    $web_a = clean_string($_POST['web_a']); $about_c = clean_string($_POST['about_c']);
    $fio = clean_string($_POST['fio']);
    if (isset($_POST['update'])){
        $db = db_connect();
        if (check($name_comp)) update_catalog($name_comp, $nameC, $adress, $number, $web_a, $about_c, $fio);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Обновление данных</title>
</head>
<body>
    <form method = "POST" align = "left" >
        Введите название компании, данные которой надо обновить<input type = "text" name = "name_comp"><br/><br/>
        Все поля обязательны к заполнению:<br/><br/>
        Название компании<input type = "text" name = "nameC"><br/><br/>
        Адресс<input type = "text" name = "adress"><br/><br/>
        Телефон<input type = "text" name = "number"><br/><br/>
        Адресс сайта<input type = "text" name = "web_a"><br/><br/>
        О компании<input type = "text" name = "about_c"><br/><br/>
        ФИО директора<input type = "text" name = "fio"><br/><br/>
        <input type = "submit" name = "update" value = "Обновить"><br/><br/>
        <a href="/"><input type= "button" value = "Перейти на главную"></a>
    </form>
</body>
</html>
