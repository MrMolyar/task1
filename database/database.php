<?php
    function db_connect(){

        $mysqli = new mysqli("localhost", "root", "", "task1");
        $mysqli -> query ("SET NAMES 'utf-8'");
        if (!$mysqli){
            echo "Ошибка невозможно установить соединение с MySql" . PHP_EOL;
            echo "Код ошибки errno:" . mysqli_connect_errno()  . PHP_EOL;
            echo "Текст ошибки error" . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        if (!mysqli_set_charset($mysqli, "utf8")) {
            printf("Ошибка при наборе символов utf-8: %s\n", mysqli_error($mysqli));
            exit;
        }
        return $mysqli;
    }

    function select_catalog(){
        $db = db_connect();
        $result = mysqli_query($db, "SELECT `name_comp`,`reg_date`,`adress`,`number_phone`,`web_adress`,`about_comp`,`fio_dir` FROM `catalog`");
        while ($row = mysqli_fetch_row($result)){
            $response[] = $row;
        }
        mysqli_close($db);
        return $response;
    }

    function add_catalog($name, $adress, $number, $web_a, $about_c, $fio_dir){
        $db = db_connect();
        mysqli_query($db, "INSERT INTO `catalog` (`name_comp`, `adress`, `number_phone`, `web_adress`, `about_comp`, `fio_dir`) VALUES ('{$name}', '{$adress}', '{$number}', '{$web_a}', '{$about_c}', '{$fio_dir}')");
        mysqli_close($db);
    }

    function delete_catalog($name_comp){
        $db = db_connect();
        mysqli_query($db, "DELETE FROM `catalog` WHERE `name_comp` = '{$name_comp}'");
        mysqli_close($db);
    }

    function update_catalog($nameUpdate, $name, $adress, $number, $web_a, $about_c, $fio_dir){
        $db = db_connect();
        mysqli_query($db, "UPDATE `catalog` SET `name_comp` = '{$name}', `adress` = '{$adress}', `number_phone` = '{$number}', `web_adress` = '{$web_a}', `about_comp` = '{$about_c}', `fio_dir` = '{$fio_dir}' WHERE `name_comp` = '{$nameUpdate}'" );
        mysqli_close($db);
    }

    function clean_string($name){
        $db = db_connect();
        $name = trim($name);
        $name = htmlspecialchars($name, ENT_NOQUOTES, 'utf-8');
        $name = mysqli_real_escape_string($db, $name);
        mysqli_close($db);
        return $name;
    }

    function check($name){
        $db = db_connect();
        $result = mysqli_query($db, "SELECT * FROM `catalog` WHERE `name_comp` = '{$name}'");
        $result = mysqli_num_rows($result);
        mysqli_close($db);
        if ($result == NULL) {
            ?><script> alert('Проверьте правильность введёных данных');</script><?php
            return false;
        }
        else return true;
    }
?>